
const fs = require('fs');
const axios = require('axios');

const data = {}

const categoryIDs = [4145, 2920, 2028, 4377, 2233, 1588, 2124, 309, 554, 1371, 4616, 254];
const apiURL = 'http://jservice.io/api/category/'

const categoryPromises = categoryIDs
    .map(
        id => axios.get(apiURL, {
            params: {
                id: id
            }
        }).then(res => res.data)
            .catch(err => err.message)
    )

Promise.all(categoryPromises)
    .then(function (categoryObjects) {
        for (let i = 0; i < categoryObjects.length; i++) {
            data[categoryObjects[i].id] = categoryObjects[i];
        }

        fs.writeFile('myjsonfile.json', JSON.stringify(data), 'utf8', (err) => {
            if(err) throw err;
            console.log('the file has been saved!');
        });
    });





